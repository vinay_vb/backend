package com.countryfinancial.marketing.lnt.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.countryfinancial.marketing.lnt.model.Opportunity;


@Service
public class OpportunityService {
	
    public List<Opportunity> getProspectsForUser(Long id, String status) {
        List<Opportunity> opportunities = new ArrayList<Opportunity>();
        if("new".equals(status)) {
        	opportunities.add(new Opportunity(123l, "Vinay"));
        	opportunities.add(new Opportunity(334l, "Joe"));
        	opportunities.add(new Opportunity(837l, "Jerry"));
        } else if ("quoting".equals(status)) {
        	opportunities.add(new Opportunity(123l, "Mike"));
        	opportunities.add(new Opportunity(334l, "Bob"));
        	opportunities.add(new Opportunity(837l, "Luke"));
        } else {
        	opportunities.add(new Opportunity(123l, "Eric"));
        	opportunities.add(new Opportunity(334l, "Smith"));
        	opportunities.add(new Opportunity(837l, "Tyler"));
        }
        return opportunities;
    }

}
