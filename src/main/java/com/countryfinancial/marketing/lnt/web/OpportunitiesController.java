package com.countryfinancial.marketing.lnt.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.countryfinancial.marketing.lnt.service.OpportunityService;
import com.countryfinancial.marketing.lnt.model.Opportunity;

@RestController
@RequestMapping("/opportunities")
public class OpportunitiesController {

    @Autowired
    OpportunityService service;

    public OpportunitiesController() {
    }

    @GetMapping("/users/{id}/opportunities")
    public ResponseEntity<List<Opportunity>> getProspects(@PathVariable Long id, @RequestParam(value = "status", required = false) String status) {
        return new ResponseEntity<>(service.getProspectsForUser(id, status),  HttpStatus.OK);
    }
}

